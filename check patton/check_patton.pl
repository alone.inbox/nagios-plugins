#!/usr/bin/perl -w
#
# check_patton.pl v1.0.1
#
# version history
#
# 1.0 first release  
#
# Nagios plugin script for checking Patton Gateways.
#
# License: GPL v2
# Copyright (c) 2017 Davide "Argaar" Foschi
#

use strict 'vars';
no warnings 'uninitialized';
use Net::SNMP qw(ticks_to_time);;
use Switch;
use Getopt::Std;
use Scalar::Util qw(looks_like_number);

# Command arguments
my %options=();
getopts("H:C:l:s:w:c:u", \%options);

# Help message etc
(my $script_name = $0) =~ s/.\///;

my $help_info = <<END;
\n$script_name - v1.0

Nagios script to check status of a Patton GateWay.

Usage:
-H  Address or hostname of Patton (required)
-C  SNMP community string (required)
-l  Command (optional, see command list)
-s  Specific command parameter
-p  SNMP port (optional, defaults to port 161)
-t  Connection timeout (optional, default 10s)
-w  Warning threshold (optional, see commands)
-c  Critical threshold (optional, see commands)
-u  Script / connection errors will return unknown rather than critical

Commands (supplied with -l argument):

    id
        The Patton comprehensive device information (model, serial number, hw/sw release version, etc..)
    hwversion
        Device Hardware Version
    swversion
        Device Software Version
    hwrelease
        Device Hardware Release
    serial
        Device Serial Number
    productname
        Device Product Name
    uptime
        Device Current Uptime
    cpucurrent
        Device CPU Current % Utilization
        --This command accepts "W" and "C" thresholds--
    cpu1min
        Device CPU Last Minute % Utilization
        --This command accepts "W" and "C" thresholds--
    cpu5min
        Device CPU Last 5 Minutes % Utilization
        --This command accepts "W" and "C" thresholds--
    memory
        ###############################################
        --This command accepts "W" and "C" thresholds--
    isdn
        Retrieve info about ISDN port attached
        This command accepts the isdn number and check as parameter (see example below) , if "S" param is omitted,
        the script will output aggregate information
        Parameters should be the isdn port number or "all" and the check you want to perform, choose from "connected", "ongoing" and "total"
        So, for example, if you want to know the total number of processed calls the parameter will be "all-total"
        Instead if you want to know only the current connected calls for the first isdn the parameter will be "1-connected"
    interface
        Retrieve the status of the attacheed port (ethernet, isdn, etc.)
        This command accepts the interface number (index) as check parameter, if "S" param is omitted,
        the script will output a summary of all interfaces found with the correspondent index so you can
        easily determine which interface you want to monitor
        This command DOES NOT accepts "W" and "C" thresholds as it can't monitor values, rather only the current status, however it returns
        UNKNOWN, OKAY, WARNING and CRITICAL based on the interface status.
        

Example:
$script_name -H pattonGw1.domain.local -C public -l isdn -s 1-total

END

# OIDs for the checks
my $oid_sysDescr                            = "1.3.6.1.2.1.1.1.0";                  # Device Name (ex : SN4961/4E1/hwRelease,swVersion...)
my $oid_sysUptime                           = "1.3.6.1.2.1.1.3.0";                  # 64 bit Integer (da moltiplicare per 0.0000027778 per ottenerlo in minuti)
my $oid_serialNumber                        = "1.3.6.1.4.1.1768.100.1.1.0";         # Provides the serial number of the device.
my $oid_hwRelease                           = "1.3.6.1.4.1.1768.100.1.3.0";         # Provides the hardware release of the device.
my $oid_hwVersion                           = "1.3.6.1.4.1.1768.100.1.4.0";         # Provides the hardware version of the device.
my $oid_swVersion                           = "1.3.6.1.4.1.1768.100.1.5.0";         # Provides the software version of the device. The following is an example of a software version information:  R4.2 2008-09-11 SIP
my $oid_productName                         = "1.3.6.1.4.1.1768.100.1.6.0";         # Provides the product name of the device.
my $oid_cpuWorkloadCurrent                  = "1.3.6.1.4.1.1768.100.70.10.2.1.2.1"; # The current CPU workload in percent.
my $oid_cpuWorkload1MinuteAverage           = "1.3.6.1.4.1.1768.100.70.10.2.1.3.1"; # The CPU workload average over the last minute in percent.
my $oid_cpuWorkload5MinuteAverage           = "1.3.6.1.4.1.1768.100.70.10.2.1.4.1"; # The CPU workload average over the last 5 minutes in percent.
my $oid_memTotalBytes                       = "1.3.6.1.4.1.1768.100.70.20.2.1.2.1"; # Il numero totale di byte appartenenti al pool di memoria.
my $oid_memFreeBytes                        = "1.3.6.1.4.1.1768.100.70.20.2.1.4.1"; # Il numero corrente di byte liberi nel pool di memoria.
my $oid_idsnNumber                          = "1.3.6.1.4.1.1768.100.70.50.1.0";     # The number of ISDN ports present on this system.
my $oid_isdnPortDescr_base                  = "1.3.6.1.4.1.1768.100.70.50.2.1.1";   # A description of the ISDN port.
my $oid_isdnPortCurrentConnectedCalls_base  = "1.3.6.1.4.1.1768.100.70.50.2.1.2";   # The total number of calls of this ISDN port that are currently in the connected state
my $oid_isdnPortCurrentOngoingCalls_base    = "1.3.6.1.4.1.1768.100.70.50.2.1.3";   # The total number of calls this ISDN port that are currently in the connected, a call setup or a call clearing state.
my $oid_isdnPortTotalAccumulatedCalls_base  = "1.3.6.1.4.1.1768.100.70.50.2.1.4";   # The total accumulated number of calls processed of this ISDN port since the system has started.
my $oid_ifNumber                            = "1.3.6.1.2.1.2.1.0";                  # The number of network interfaces (regardless of their current state) present on this system.
my $oid_ifDescr_base                        = "1.3.6.1.2.1.2.2.1.2";                # A textual string containing information about the interface.  This string should include the name of the manufacturer,
                                                                                    # the product name and the version of the hardware interface.
my $oid_ifOperStatus_base                   = "1.3.6.1.2.1.2.2.1.8";                # The current operational state of the interface. The testing(3) state indicates that no operational packets can be passed.

# Nagios exit codes
my $OKAY        = 0;
my $WARNING     = 1;
my $CRITICAL    = 2;
my $UNKNOWN     = 3;

# Command arguments and defaults
my $snmp_host           = $options{H};
my $snmp_community      = $options{C};
my $snmp_port           = $options{p} || 161;   # SNMP port default is 161
my $connection_timeout  = $options{t} || 10;    # Connection timeout default 10s
my $default_error       = (!defined $options{u}) ? $CRITICAL : $UNKNOWN;
my $check_command       = $options{l};
my $command_params      = $options{s};
my $critical_threshold  = $options{c};
my $warning_threshold   = $options{w};
my $session;
my $error;
my $exitCode;

#Maximum length of 15 characters for snmp community strings
if(defined $snmp_community) {$snmp_community = substr($snmp_community,0,15);}

# If we don't have the needed command line arguments exit with UNKNOWN.
if(!defined $options{H} || !defined $options{C}){
    print "$help_info\n\n--> ERROR <--\n--> Not all required options were specified. <--\n\n";
    exit $UNKNOWN;
}

# Setup the SNMP session
($session, $error) = Net::SNMP->session(
    -hostname   => $snmp_host,
    -community  => $snmp_community,
    -timeout    => $connection_timeout,
    -port       => $snmp_port,
    -translate  => [-timeticks => 0x0]
);

# If we cannot build the SNMP session, error and exit
if (!defined $session) {
    my $output_header = ($default_error == $CRITICAL) ? "CRITICAL" : "UNKNOWN";
    printf "$output_header: %s\n", $error;
    exit $default_error;
}

# Determine what we need to do based on the command input
if (!defined $options{l}) {  # If no command was given, just output the UPS model
    my $patton_model = query_oid($oid_productName);    
    $session->close();
    print "$patton_model\n";
    exit $OKAY;
} else {    # Process the supplied command. Script will exit as soon as it has a result.
    switch($check_command){

        case "id" {
            my $patton_model = query_oid($oid_productName); 
            my $patton_sn    = query_oid($oid_serialNumber);
            my $patton_sysd  = query_oid($oid_sysDescr);
            my $patton_swv   = query_oid($oid_swVersion);
            $session->close();
            print "OK: Patton Model: $patton_model, Firmware: $patton_swv, S/N: $patton_sn, System Summary: $patton_sysd\n";
            exit $OKAY;
        }
        case "hwversion" {
            my $patton_hwv = query_oid($oid_hwVersion);
            $session->close();
            print "OK: Patton Hardware Version: $patton_hwv\n";
            exit $OKAY;
        }
        case "swversion" {
            my $patton_swv = query_oid($oid_swVersion);
            $session->close();
            print "OK: Patton Software Version: $patton_swv\n";
            exit $OKAY;
        }
        case "hwrelease" {
            my $patton_hwr = query_oid($oid_hwRelease);
            $session->close();
            print "OK: Patton Hsrdware Release: $patton_hwr\n";
            exit $OKAY;
        }
        case "serial" {
            my $patton_sn = query_oid($oid_serialNumber);
            $session->close();
            print "OK: Patton Serial Number: $patton_sn\n";
            exit $OKAY;
        }
        case "productname" {
            my $patton_pn = query_oid($oid_productName);
            $session->close();
            print "OK: Patton Product Name: $patton_pn\n";
            exit $OKAY;
        }
        case "uptime" {
            my $patton_up = query_oid($oid_sysUptime);
            $session->close();
            my @upTimeCalc = split(/\./, $patton_up / 100 / 3600 / 24);
            my $days = $upTimeCalc[0];
            my $hours = sprintf "%.0f", (24 * ("0.".$upTimeCalc[1])-1);
            if ($hours<0) {
                $hours = 0;
            }
            print "OK: Patton Uptime: $days day(s) and $hours hour(s)\n";
            exit $OKAY;
        }
        case "cpucurrent" {
            my $patton_cpuCurrent = query_oid($oid_cpuWorkloadCurrent);
            $session->close();
            if (defined $critical_threshold && defined $warning_threshold && $critical_threshold<$warning_threshold) {
                print "ERROR: Critical Threshold should be GREATER than Warning Threshold!\n";
                $exitCode = $UNKNOWN;
            } else {
                if (defined $critical_threshold && $patton_cpuCurrent >= $critical_threshold){
                    print "CRITICAL: CPU Current Utilization $patton_cpuCurrent is HIGHER or Equal than the critical threshold of $critical_threshold";
                    $exitCode = $CRITICAL;
                } elsif (defined $warning_threshold && $patton_cpuCurrent >= $warning_threshold){
                    print "WARNING: CPU Current Utilization $patton_cpuCurrent is HIGHER or Equal than the warning threshold of $warning_threshold";
                    $exitCode = $WARNING;
                }else{
                    print "OK: CPU Current Utilization is: $patton_cpuCurrent";
                    $exitCode = $OKAY; 
                }
                print "|'CPU Current'=$patton_cpuCurrent".";$warning_threshold;$critical_threshold\n";
            }
            exit $exitCode;
        }
        case "cpu1min" {
            my $patton_cpu1min = query_oid($oid_cpuWorkload1MinuteAverage);
            $session->close();
            if (defined $critical_threshold && defined $warning_threshold && $critical_threshold<$warning_threshold) {
                print "ERROR: Critical Threshold should be GREATER than Warning Threshold!\n";
                $exitCode = $UNKNOWN;
            } else {
                if (defined $critical_threshold && $patton_cpu1min >= $critical_threshold){
                    print "CRITICAL: CPU 1 Min Utilization $patton_cpu1min is HIGHER or Equal than the critical threshold of $critical_threshold";
                    $exitCode = $CRITICAL;
                } elsif (defined $warning_threshold && $patton_cpu1min >= $warning_threshold){
                    print "WARNING: CPU 1 Min Utilization $patton_cpu1min is HIGHER or Equal than the warning threshold of $warning_threshold";
                    $exitCode = $WARNING;
                }else{
                    print "OK: CPU 1 Min Utilization is: $patton_cpu1min";
                    $exitCode = $OKAY; 
                }
                print "|'CPU 1 Min'=$patton_cpu1min".";$warning_threshold;$critical_threshold\n";
            }
            exit $exitCode;
        }
        case "cpu5min" {
            my $patton_cpu5min = query_oid($oid_cpuWorkload5MinuteAverage);
            $session->close();
            if (defined $critical_threshold && defined $warning_threshold && $critical_threshold<$warning_threshold) {
                print "ERROR: Critical Threshold should be GREATER than Warning Threshold!\n";
                $exitCode = $UNKNOWN;
            } else {
                if (defined $critical_threshold && $patton_cpu5min >= $critical_threshold){
                    print "CRITICAL: CPU 5 Min Utilization $patton_cpu5min is HIGHER or Equal than the critical threshold of $critical_threshold";
                    $exitCode = $CRITICAL;
                } elsif (defined $warning_threshold && $patton_cpu5min >= $warning_threshold){
                    print "WARNING: CPU 5 Min Utilization $patton_cpu5min is HIGHER or Equal than the warning threshold of $warning_threshold";
                    $exitCode = $WARNING;
                }else{
                    print "OK: CPU 5 Min Utilization is: $patton_cpu5min";
                    $exitCode = $OKAY; 
                }
                print "|'CPU 5 Min'=$patton_cpu5min".";$warning_threshold;$critical_threshold\n";
            }
            exit $exitCode;
        }
        #case "memory" {
        #    exit $exitCode;
        #}
        case "isdn" {
            if (!defined $command_params) { $command_params = "all-total"; };
            my $isdn_count = query_oid($oid_idsnNumber);
            my @command_params_splitted = split(/\-/, $command_params);
            my $isdn_id = $command_params_splitted[0];
            my $isdn_check = $command_params_splitted[1];
            switch ($isdn_check) {
                case "ongoing" {
                    if ( $isdn_id eq "all" ) {
                        my $isdn_ongoing_all = 0;
                        for (my $i=1; $i<=$isdn_count; $i++) {
                            $isdn_ongoing_all += query_oid($oid_isdnPortCurrentOngoingCalls_base.".$i");
                        }
                        print "OK: Ongoing Calls Total is $isdn_ongoing_all|'Ongoing Calls Total'=$isdn_ongoing_all\n";
                        $exitCode = $OKAY;
                    } else {
                        my $isdn_ongoing = query_oid($oid_isdnPortCurrentOngoingCalls_base.".$isdn_id");
                        print "OK: Ongoing Calls on Isdn$isdn_id is $isdn_ongoing|'Ongoing Calls Isdn$isdn_id'=$isdn_ongoing\n";
                        $exitCode = $OKAY;
                    }
                }
                case "connected" {
                    if ( $isdn_id eq "all" ) {
                        my $isdn_connected_all = 0;
                        for (my $i=1; $i<=$isdn_count; $i++) {
                            $isdn_connected_all += query_oid($oid_isdnPortCurrentConnectedCalls_base.".$i");
                        }
                        print "OK: Connected Calls Total is $isdn_connected_all|'Connected Calls Total'=$isdn_connected_all\n";
                        $exitCode = $OKAY;
                    } else {
                        my $isdn_connected = query_oid($oid_isdnPortCurrentConnectedCalls_base.".$isdn_id");
                        print "OK: Connected Calls on Isdn$isdn_id is $isdn_connected|'Connected Calls Isdn$isdn_id'=$isdn_connected\n";
                        $exitCode = $OKAY;
                    }
                }
                case "total" {
                    if ( $isdn_id eq "all" ) {
                        my $isdn_total_all = 0;
                        for (my $i=1; $i<=$isdn_count; $i++) {
                            $isdn_total_all += query_oid($oid_isdnPortTotalAccumulatedCalls_base.".$i");
                        }
                        print "OK: Total Calls of all Isdn is $isdn_total_all|'Total Calls All Isdn'=$isdn_total_all\n";
                        $exitCode = $OKAY;
                    } else {
                        my $isdn_total = query_oid($oid_isdnPortTotalAccumulatedCalls_base.".$isdn_id");
                        print "OK: Total Calls on Isdn$isdn_id is $isdn_total|'Total Calls Isdn$isdn_id'=$isdn_total\n";
                        $exitCode = $OKAY;
                    }
                } else {
                    print "$script_name - '$isdn_check' is not a valid isdn check\n"; 
                    $exitCode = $UNKNOWN;
                }
            }
            $session->close();
            exit $exitCode;
        }
        case "interface" {
            my $patton_intfnum = query_oid($oid_ifNumber);
            my @intf_status_text = ("UP","DOWN","TESTING","UNKNOWN","DORMANT","NON PRESENT","LOW LAYER DOWN");
            if (!defined $command_params) {
                print "Total interfaces found: $patton_intfnum\n";
                for (my $i=1; $i<=$patton_intfnum; $i++) {
                    my $intf_name = query_oid($oid_ifDescr_base . "." . $i);
                    my $intf_status = query_oid($oid_ifOperStatus_base . "." . $i);
                    print "Index: $i - Name: $intf_name - Status: $intf_status_text[$intf_status-1]\n";
                }
                $exitCode = $OKAY;
            } else {
                if (looks_like_number($command_params)) { 
                    my $intf_name = query_oid($oid_ifDescr_base . "." . $command_params);
                    my $intf_status = query_oid($oid_ifOperStatus_base . "." . $command_params);
                    switch ($intf_status) {
                        case "1" {
                            $exitCode = $OKAY;
                            print "OK: ";
                        }
                        case "2" {
                            $exitCode = $CRITICAL;
                            print "CRITICAL: ";
                        }
                        case "3" {
                            $exitCode = $WARNING;
                            print "WARNING: ";
                        }
                        case "4" {
                            $exitCode = $UNKNOWN;
                            print "UNKNOWN: ";
                        }
                        case "5" {
                            $exitCode = $WARNING;
                            print "WARNING: ";
                        }
                        case "6" {
                            $exitCode = $UNKNOWN;
                            print "UNKNOWN: ";
                        }
                        case "7" {
                            $exitCode = $CRITICAL;
                            print "CRITICAL: ";
                        }
                    }
                    print "Index: $command_params - Name: $intf_name - Status: $intf_status_text[$intf_status-1]\n";
                } else {
                    print "Interface number '$command_params' has not been recognized as a number, please correct the issue\n";
                    $exitCode = $UNKNOWN;
                }
            }
            $session->close();
            exit $exitCode;
        }
        else {
            print "$script_name - '$check_command' is not a valid comand\n";
            exit $UNKNOWN;
        }

    }
}

sub query_oid {
# This function will poll the active SNMP session and return the value
# of the OID specified. Only inputs are OID. Will use global $session 
# variable for the session.
    my $oid = $_[0];
    my $response = $session->get_request(-varbindlist => [ $oid ],);

    # If there was a problem querying the OID error out and exit
    if (!defined $response) {
        my $output_header = ($default_error == $CRITICAL) ? "CRITICAL" : "UNKNOWN";
        printf "$output_header: %s\n", $session->error();
        $session->close();
        exit $default_error;
    }

    return $response->{$oid};
}

# The end. We shouldn't get here, but in case we do exit unknown
print "UNKNOWN: Unknown script error\n";
exit $UNKNOWN;